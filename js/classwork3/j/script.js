"use strict";

/*let firstNumber;
do {
  firstNumber = prompt("Enter the first number");
} while (
  !firstNumber ||
  typeof +firstNumber !== "number" ||
  isNaN(+firstNumber) ||
  firstNumber === ""
);

let secondNumber;
do {
  secondNumber = prompt("Enter the second number");
} while (
  !secondNumber ||
  typeof +secondNumber !== "number" ||
  isNaN(+secondNumber) ||
  secondNumber === ""
);
console.log(`Поздравляем. Ввёденные вами числа: " ${firstNumber} " `); */

let name;
let surname;
let birth;
do {
  name = prompt("Введите свое имя");
  surname = prompt("Введите свою фамилию");
} while (!name || name.trim() === "" || !surname || surname.trim() === "");

do {
  birth = prompt("Введите год своего рождения");
} while (
  !birth ||
  typeof +birth !== "number" ||
  isNaN(+birth) ||
  birth.trim() === "" ||
  birth > 2021 ||
  birth < 1910
);

console.log(` Здравствуй, рождённый в ${birth} году, ${name} ${surname}`);
