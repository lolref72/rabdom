"use strict";
/*
"Hi";
const str = "Hi";
const num1 = 2;
const num2 = 3;

//указываем в () - параметры (парметры, которые находятся вне функции)

function sum(a, b) {
  let result = a + b;
  return result;
} //ЧИСТАЯ ФУНКЦИЯ. ПРИНИМАЕТ ПАРАМЕТРЫ, ВОЗВРАЩАЕТ РЕЗУЛЬТАТ. НЕ ИЗМЕНЯЯ ВНЕШНИЕ ДАННЫЕ

const sum2 = function fnName() {
  return 2 + 2;
};

console.log(sum(num1, num2)); //указываем в () - аргументы.
console.log(sum2());
//всплытие. все функции в формате function.declaration считываются ПЕРВЫМИ. значит, вызывать их можно до инициализации
//функции нужны чтобы не дублировать код. если действие повторяется несколько раз, то чтобы не прописывать действие снова и снова, можно просто прописать функцию и вызывать её по необходимости

//в названнии функции должен присутсоввать глагол
function recursionCreate(value) {
  value++;
  console.log(value);
  if (value < 30) {
    recursionCreate(value);
  }
}

recursionCreate(3); */

// /* function recursionCreate(value = 20) {
//   console.log(arguments);
//   console.log(arguments.length);

//   for (let index = 0; index < arguments; index++) {
//     console.log(arguments[index]);
//   }
// }
// recursionCreate("first", "second", "third", "KOLYAPIDOR");
//  */

//TASK 1
/* let numberOne = prompt("NUMBER ONE");
let numberTwo = prompt("NUMBER TWO");
function sum(a, b) {
  return a + b;
}

console.log(sum(+numberOne, +numberTwo));  */

//TASK 2

// function calculateArguments() {
//   return arguments.length;
// }
// console.log(calculateArguments(2, 2, 3, 4, 5, 6));

//TASK 3

// let beginning = prompt("ЧИСЛО НАЧАЛА ОТСЧЕТА");
// let ending = prompt("ЧИСЛО ОКОНЧАНИЯ ОТСЧЁТА");

// function numberToNumberCount(number1, number2) {
//   console.log(number1);
//   number1++;
//   if (number1 > number2) {
//     numberToNumberCount(beginning, ending);
//   }
// }

// console.log(numberToNumberCount(beginning, ending));

// function count(num1, num2) {
//   if (num2 < num1) {
//     console.log("⛔️ Ошибка! Счёт невозможен.");
//   } else if (num1 === num2) {
//     console.log("⛔️ Ошибка! Нечего считать.");
//   } else {
//     console.log("🏁 Отсчёт начат.");
//     for (num1; num1 <= num2; num1++) {
//       console.log(num1);
//     }
//     console.log("✅ Отсчёт завершен.");
//   }
// }

// count(1, 5);

//TASK 4
// function count(num1, num2, num3) {
//   if (
//     typeof num1 === "number" &&
//     typeof num2 === "number" &&
//     typeof num3 === "number" &&
//     num1 !== null &&
//     num2 !== null &&
//     num3 !== null &&
//     !isNaN(+num1) &&
//     !isNaN(+num2) &&
//     !isNaN(+num3)
//   ) {
//     if (num2 < num1) {
//       console.log("⛔️ Ошибка! Счёт невозможен.");
//     } else if (num1 === num2) {
//       console.log("⛔️ Ошибка! Нечего считать.");
//     } else {
//       console.log("🏁 Отсчёт начат.");
//       for (num1; num1 <= num2; num1++) {
//         console.log(num1);
//       }
//       console.log("✅ Отсчёт завершен.");
//     }
//   } else if (arguments.length < 3) {
//     console.log("MORE NUMBERS");
//   } else {
//     console.log("type numbers");
//   }
// }

// count(1, 10, 3);

//TASK 5

function sum() {
  if (arguments.length < 2) {
    console.log("Введите больше значений");
    return;
  }
  for (let index = 0; index < arguments.length; index++)
    if (
      typeof arguments[index] === "number" &&
      arguments[index] !== null &&
      !isNaN(+arguments[index])
    ) {
      console.log("ok");
    } else {
      console.log(`Ошибка в значении номер: ` + (index + 1));
    }

  console.log(arguments.length);
}
sum(1, 2, 3, 6, "aaa");
