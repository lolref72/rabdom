"use strict";
// переменные созданные с var могут быть повторно объявлены           (var = "hi";
//                                                                    var = "bye";) и это не будет ошибкой

// глобальная переменная, созданная с var будет обновляться при её использованнии в локальной области видимости (внутри фигурных скобок)

//при "всплытии" (поднятие компилятором переменных и функций вверх), переменные созданные с let, в отличии от созданных с var, не будут возращать значение undefiend

//переменные созданные с const, в отличии от созданных с var или let, должны быть инициализированы при объявлении

//переменные созданные с const, не могут быть обновлены. но могут быть изменены её значения внутри функции
const name = prompt("Please enter your name");
const age = prompt("Please enter your age");

if (+age < 18) {
  alert(`You are not allowed to visit this website`);
}

if (+age > 22) {
  alert(`Welcome, ${name}`);
}

if (+age === 18 || +age === 19 || +age === 20 || +age === 21 || +age === 22) {
  let approval0 = confirm(`Are you sure you want to enter the site?`);
  if (approval0) {
    alert(`Welcome, ${name}`);
  } else alert(`You are not allowed to visit this website`);
}
